// We only load plugins here
// Plugins load the modules
// modules load the rest of the functionality

import fs from 'fs';
import path from 'path';

const plugin_dir = path.join(__dirname,'./plugins');

const plugins = fs.readdirSync(plugin_dir).filter(
	file => file.endsWith('.ts')
);

for(const file of plugins){
	require(path.join(plugin_dir,file));
}
