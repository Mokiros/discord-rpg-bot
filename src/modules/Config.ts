import {config} from 'dotenv';
config();

export default {
    bot_token: process.env.discord_bot_token,
    prefix: '!',
    separator: ' '
}