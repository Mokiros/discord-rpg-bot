import fs from 'fs';
import path from 'path';

import Command from '../types/Commands';

const Commands:Map<string,Command> = new Map();

export function GetCommand(name:string): Command|undefined {
    return Commands.get(name);
}

export async function LoadCommands():Promise<void>{
    const commands_dir = path.join(__dirname,'../commands');
    const commandFiles = fs.readdirSync(commands_dir).filter(
        file => file.endsWith('.ts')
    );
    for (const file of commandFiles) {
        const command:Command = require(path.join(commands_dir,file));
        const cmd:Array<string> | string = command.CommandName;
        if(cmd instanceof Array){
            for(const name of cmd){
                Commands.set(name, command);
            }
        }else if(typeof cmd === 'string'){
            Commands.set(cmd, command);
        }
    }
}
