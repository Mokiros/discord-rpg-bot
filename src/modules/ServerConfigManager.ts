import {Collection} from 'discord.js';

interface ServerConfig {
	ServerId: string,
	ReactionMessages: Collection<string,Collection<string,string>>
}

export default class Server {
	private config: ServerConfig
	public prefix: string

	constructor(ServerId: string, prefix?:string){
		this.prefix = prefix ?? '!';
		this.config = {
			ServerId: ServerId,
			ReactionMessages: new Collection()
		};
	}

	getReactionMessages(){
		return this.config.ReactionMessages;
	}

	addReactionRole(MessageId:string,ReactionId:string,RoleId:string){
		const Message = this.config.ReactionMessages
	}
}