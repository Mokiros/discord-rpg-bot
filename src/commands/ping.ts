import {Message} from 'discord.js';
import Command, {Arguments} from '../types/Commands';

interface CommandArguments extends Arguments {

}

const cmd:Command = {
	CommandName: 'ping',
	run(args:CommandArguments,message:Message){
		return 'Pong!';
	}
}

module.exports = cmd;