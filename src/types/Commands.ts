import Discord from 'discord.js';

export enum ArgumentTypes {
    "string",
    "number",
    "boolean",
    "GuildMember",
    "Channel",
    "Role"
}

export interface Arguments {
    [id:string]: any
    /*
        string |
        number |
        boolean |
        Discord.GuildMember |
        Discord.Channel |
        Discord.Message |
        Discord.Role |
        undefined
    */
}

export interface CommandArgument {
    id: string,
    type: ArgumentTypes,
    required?: boolean
}

export default interface Command {
	CommandName: Array<string> | string,
	Arguments?: Array<CommandArgument>,
	run(args:Arguments,message:Discord.Message): string | void
}